set(TARGET ldm-common)

set(SOURCES
	lomiri/transfers/metadata.cpp
	lomiri/transfers/errors/auth_error_struct.cpp
	lomiri/transfers/errors/hash_error_struct.cpp
	lomiri/transfers/errors/http_error_struct.cpp
	lomiri/transfers/errors/network_error_struct.cpp
	lomiri/transfers/errors/process_error_struct.cpp
	lomiri/transfers/system/dbus_connection.cpp
	lomiri/transfers/system/hash_algorithm.cpp
	lomiri/transfers/system/logger.cpp
)

set(TRANSFERS_PUBLIC_HEADERS
	lomiri/transfers/metadata.h
	lomiri/transfers/visibility.h
)

set(ERRORS_PUBLIC_HEADERS
	lomiri/transfers/errors/auth_error_struct.h
	lomiri/transfers/errors/hash_error_struct.h
	lomiri/transfers/errors/http_error_struct.h
	lomiri/transfers/errors/network_error_struct.h
	lomiri/transfers/errors/process_error_struct.h
)

set(PRIVATE_HEADERS
	lomiri/transfers/system/dbus_connection.h
	lomiri/transfers/system/hash_algorithm.h
	lomiri/transfers/system/logger.h
)

include_directories(${Qt5DBus_INCLUDE_DIRS})
include_directories(${Qt5Network_INCLUDE_DIRS})

add_library(${TARGET} SHARED
	${SOURCES}
	${TRANSFERS_PUBLIC_HEADERS}
	${ERRORS_PUBLIC_HEADERS}
	${PRIVATE_HEADERS}
)

set(symbol_map "${CMAKE_SOURCE_DIR}/src/common/public/symbols.map")
set_target_properties(
	${TARGET}

	PROPERTIES
        LINK_FLAGS "${ldflags} -Wl,--version-script,${symbol_map}"
        LINK_DEPENDS ${symbol_map}
	VERSION ${LDM_VERSION_MAJOR}.${LDM_VERSION_MINOR}.${LDM_VERSION_PATCH}
	SOVERSION ${LDM_VERSION_MAJOR}
)

target_link_libraries(${TARGET}
	${GLOG_LIBRARIES}
	${Qt5DBus_LIBRARIES}
	${Qt5Network_LIBRARIES}
)

configure_file(${TARGET}.pc.in ${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.pc @ONLY)

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TARGET}.pc DESTINATION ${CMAKE_INSTALL_LIBEXECDIR}/pkgconfig)
install(TARGETS ${TARGET} DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(FILES ${TRANSFERS_PUBLIC_HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/lomiri/transfers)
install(FILES ${ERRORS_PUBLIC_HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/lomiri/transfers/errors)
