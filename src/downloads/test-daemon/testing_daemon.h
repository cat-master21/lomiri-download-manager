/*
 * Copyright 2013-2015 Canonical Ltd.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of version 3 of the GNU Lesser General Public
 * License as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#pragma once

#include <QObject>
#include <lomiri/download_manager/metatypes.h>
#include <lomiri/downloads/daemon.h>  // comes from the priv lib, just for testing!!!!

using namespace Lomiri::Transfers;
using namespace Lomiri::System;

class TestingManagerAdaptor;
class TestingDaemon : public Lomiri::Transfers::BaseDaemon {
    Q_OBJECT
 public:
    explicit TestingDaemon(QObject *parent = 0);
    virtual ~TestingDaemon();

    // let the client test to tell the manager to return dbus errors
    void returnDBusErrors(bool errors);
    void returnAuthError(const QString &download, AuthErrorStruct error);
    void returnHashError(const QString &download, HashErrorStruct error);
    void returnHttpError(const QString &download, HttpErrorStruct error);
    void returnNetworkError(const QString &download, NetworkErrorStruct error);
    void returnProcessError(const QString &download, ProcessErrorStruct error);

    QString daemonPath();
    void setDaemonPath(QString path);

 public slots:
    void start(const QString& path="com.lomiri.applications.testing.Downloader") override;

 private:
    QString _path;
    TestingManagerAdaptor* _testsAdaptor = nullptr;

};

